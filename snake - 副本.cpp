#include <stdio.h>
#include <windows.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>
#include "graphics.h"
#include "easyx.h"
#include <math.h>


								//Bug解决指南
#pragma warning(disable:4996)   //https://blog.csdn.net/qq_35307005/article/details/87906208
#undef UNICODE                  //https://blog.csdn.net/qq_44256828/article/details/89279697
								//https://blog.csdn.net/love_0_love/article/details/120024094


#define Width 640
#define Height 480
#define Size 20//蛇的尺寸
#define Fsize 6//食物尺寸
//变量的定义
IMAGE image;
int i, j, k;//循环变量
int position[Width][Height];
int body;//蛇的长度
int grade;
int x_site[500]; int y_site[500];
int pace;//与速度有关的变量
int Speed;//蛇的速度
char ch;//方向键
char key;//与方向有关的变量

int Famount;//食物数量
int x_food[30]; int y_food[30];
int Fspeed;//生成食物速度
int Foodk[100];//与生成速度有关的变量

int No=1;

//view1的变量
IMAGE photo1;
char arr1[] = "选择你的账户";
//view2的变量
int No_v2 = 1;
IMAGE photo2;
struct user
{
	char name[20];
};
int count;
char ch20[20];
//view3的变量
IMAGE photo3;
char arr3[] = "开始游戏";
char arr4[] = "排行榜";
char arr5[] = "账户";
char v3ch;
char v3name[20];
int v4open = 0;
//view4的变量
IMAGE photo4;
int backv4 = 0;
char arr6[] = "Top10";
struct LIST
{
	char name[20];
	int score;
	char Score[10];
};
char arr7[] = "Back";
//
//函数声明
void View1();
void View2();
void View3();
void View4();
void Startup();
void Show();
void Inputupdate();
void NoInputupdate();
void Die();
int main()
{
	View1();
	View2();

	while (1)
	{
		View3();
		if (v4open)
			View4();

		Startup();
		while (No)
		{
			Show();
			Inputupdate();
			NoInputupdate();

		}

		if(No == 0)
			break;
		//closegraph();
	}
}
//函数
void Startup()
{
	srand((unsigned)time(NULL));

	initgraph(Width, Height);//窗口
	body = 20;//蛇
	grade = 0;
	x_site[0] = Width / 2; y_site[0] = Height / 2;
	for (i = 1; i < body; i++)
	{
		x_site[i] = Width / 2;
		y_site[i] = Height / 2 - i;
	}
	for (i = 1; i < body; i++)
		position[x_site[i]][y_site[i]] = 1;
	pace = 0;
	Speed = 0;
	ch = 's';

	Fspeed = 100;
	Famount = 30;
	for (i = 0; i < Famount; i++)
		Foodk[i] = rand() % (Fspeed - 1) + 1;

	for (i = 0; i < Famount; i++)
	{
		x_food[i] = rand() % (Width - 2 - Size / 2) + 1;
		y_food[i] = rand() % (Height - 2 - Size / 2) + 1;
		position[x_food[i]][y_food[i]] = 2;
	}

}
void Show()
{
	loadimage(&image, _T("..\\source\\love.jpg"), 640, 480);//加载图片
	putimage(0, 0, &image);
	//cleardevice();
	//setbkcolor(RGB(0, 255, 255));

	BeginBatchDraw(); // 开始批量绘制

	char grade1[10];
	sprintf_s(grade1, _T("%d"), grade);
    outtextxy(0,0,grade1);

	for (i = 0; i < Width; i++)
	{
		for (j = 0; j < Height; j++)
		{
			if (position[i][j]==2)
			{
				setfillcolor(RGB(i % 256, j % 256, (i % 256 + j % 256) / 2));
				solidrectangle((i - Fsize / 2), (j - Fsize / 2), (i + Fsize / 2), (j + Fsize / 2));
			}
			if (position[i][j] == 1)
			{
				setfillcolor(RGB(i % 256, j % 256, (i % 256 + j % 256) / 2));
				solidcircle(i, j, Size / 2);
			}
			if (i == x_site[0] && j == y_site[0])
			{
				setfillcolor(GREEN);
				solidcircle(x_site[0], y_site[0], Size / 2);
			}

		}
	}
	FlushBatchDraw(); // 批量绘制
}
void Inputupdate()
{
	if (_kbhit())
	{
		key = _getch();
		if (key == 'a' && ch != 'd') { ch = key; }
		if (key == 'd' && ch != 'a') { ch = key; }
		if (key == 'w' && ch != 's') { ch = key; }
		if (key == 's' && ch != 'w') { ch = key; }
	}
}
void NoInputupdate()
{

	if (pace == Speed)
	{
		if (ch == 'a') { x_site[0]--; }
		if (ch == 'd') { x_site[0]++; }
		if (ch == 'w') { y_site[0]--; }
		if (ch == 's') { y_site[0]++; }
		pace = 0;
		position[x_site[body]][y_site[body]] = 0;
		for (i = body; i > 0; i--)
		{

			x_site[i] = x_site[i - 1];
			y_site[i] = y_site[i - 1];
			position[x_site[i]][y_site[i]] = 1;
		}
	}
	else
		pace++;

	for (i = 0; i < Famount; i++)
	{
		//蛇吃到食物
		if ((x_site[0] - x_food[i]) * (x_site[0] - x_food[i]) +
			(y_site[0] - y_food[i]) * (y_site[0] - y_food[i]) <= Size / 2 * Size / 2)
		{
			position[x_food[i]][y_food[i]] = 0;
			x_food[i] = 10000; y_food[i] = 10000;
			body += 2;
			grade+=2;
		}
		//生成在蛇上的食物去掉
		/*for (j = 10; j < body; j++)
		{
			if ((x_site[j] - x_food[i]) * (x_site[j] - x_food[i]) +
				(y_site[j] - y_food[i]) * (y_site[j] - y_food[i]) <= (Size / 2 + Fsize / 2) * (Size / 2 + Fsize / 2))
			{
				position[x_food[i]][y_food[i]] = 0;
				x_food[i] = 10000; y_food[i] = 10000;
			}
		}*/
		//生成食物
		if (x_food[i] == 10000)
		{
			if (Foodk[i] == Fspeed)
			{
				x_food[i] = rand() % (Width - 2 - Fsize) + 1;
				y_food[i] = rand() % (Height - 2 - Fsize) + 1;
				position[x_food[i]][y_food[i]] = 2;
				Foodk[i] = rand() % (Fspeed - 1) + 1;
			}
			else
				Foodk[i]++;
		}
	}
	struct LIST list[10];
	int g;

	/*
	FILE* fp;
	fp=fopen(".\\source\\list.txt","r");
	for(i=0;i<10;i++)
    {
        fscanf(fp,"%s%d",list[i].name,&list[i].score);
        if(grade>list[i].score)
        {
            g=i;
			fclose(fp);
			fp= fopen(".\\source\\list.txt", "w");//图片文件
			for (i = 10; i > g; i--)
			{
				list[i].score = list[i - 1].score;
				strcpy(list[i - 1].name, list[i].name);
			}
			list[g].score = grade;
			strcpy(list[g].name, v3name);

			for (i = 0; i < 10; i++)
			{
				fprintf(fp, "%s\t%d\n", list[i].name, list[i].score);
			}
			fclose(fp);
        }
    }
	fclose(fp);
	*/

    /*fp=fopen(".\\source\\list.txt","w");
    for(i=10;i>g;i--)
    {
        list[i].score=list[i-1].score;
    }
    list[g].score=grade;*/

    /*for(i=0;i<10;i++)
    {
        fprintf(fp,"%s\t%d\n",list[i].name,list[i].score);
    }*/

	Die();
}
void Die()
{
	//撞墙
	if ((x_site[0] - Size / 2 == 0) || (x_site[0] + Size / 2 == Width)
		|| (y_site[0] - Size / 2 == 0) || (y_site[0] + Size / 2 == Height))
	{
		No = 0;
	}
	//吃到自己
	/*for (i = Size+12; i < body; i++)
	{
		if ((x_site[0] - x_site[i]) * (x_site[0] - x_site[i]) +
			(y_site[0] - y_site[i]) * (y_site[0] - y_site[i])<=Size*Size)
		{
			No = 0;
		}
	}*/
}

void View1()
{
	initgraph(Width, Height);
	loadimage(&photo1, _T("..\\source\\love.jpg"), Width, Height);
	putimage(0, 0, &photo1);

	settextcolor(BLACK);

	while (1)
	{
		settextstyle(35, 20, "楷体");
		setbkmode(TRANSPARENT);
		outtextxy(Width / 2 - textwidth(arr1) / 2, Height / 2 - textheight(arr1) / 2 - 70, arr1);


		if (MouseHit())
		{
			settextstyle(35, 20, "楷体");
			setbkmode(TRANSPARENT);
			outtextxy(Width / 2 - textwidth(arr1) / 2, Height / 2 - textheight(arr1) / 2 - 70, arr1);

			MOUSEMSG mouse1 = GetMouseMsg();
			if (mouse1.mkLButton && mouse1.x >= (Width / 2 - textwidth(arr1) / 2) && mouse1.y >= (Height / 2 - textheight(arr1) / 2 - 70)
				&& mouse1.x <= (Width / 2 + textwidth(arr1) / 2) && mouse1.y <= (Height / 2 + textheight(arr1) / 2 - 70))
			{
				FlushMouseMsgBuffer();
				break;
			}

			MOUSEMSG mouse2 = GetMouseMsg();
			if (mouse2.uMsg == WM_MOUSEMOVE)
				if (mouse1.x >= (Width / 2 - textwidth(arr1) / 2) && mouse1.y >= (Height / 2 - textheight(arr1) / 2 - 70)
					&& mouse1.x <= (Width / 2 + textwidth(arr1) / 2) && mouse1.y <= (Height / 2 + textheight(arr1) / 2 - 70))
					settextcolor(YELLOW);
				else
					settextcolor(BLACK);
		}
		FlushMouseMsgBuffer();
	}
}
void View2()
{
	initgraph(Width, Height);
	loadimage(&photo2, _T("..\\source\\love.jpg"), Width, Height);
	putimage(0, 0, &photo2);

	settextcolor(BLACK);

	int i;
	FILE* fp;
	struct user User[5];
	if ((fp = fopen("..\\source\\username.txt", "r")) == NULL)
	{
		for (i = 0; i < 5; i++)
		{
			strcpy(User[i].name, "ERROR");
		}
	}
	else
	{
		for (i = 0; i < 5; i++)
		{
			fscanf(fp, "%s", User[i].name);
		}
	}
	fclose(fp);
	settextcolor(BLACK);
	settextstyle(35, 20, "楷体");
	setbkmode(TRANSPARENT);
	for (i = 0; i < 5; i++)
	{
		outtextxy(Width / 2 - textwidth(User[i].name) / 2, Height / 2 - textheight(User[i].name) / 2 + 120 - (6 - i) * 40, User[i].name);
	}
	while (No_v2)
	{
		for (i = 0; i < 5; i++)
		{
			if (MouseHit())
			{
				MOUSEMSG mouse1 = GetMouseMsg();
				if (mouse1.mkLButton && mouse1.x >= (Width / 2 - textwidth(User[i].name) / 2) && mouse1.y >= (Height / 2 - textheight(User[i].name) / 2 + 120 - (6 - i) * 40)
					&& mouse1.x <= (Width / 2 + textwidth(User[i].name) / 2) && mouse1.y <= (Height / 2 + textheight(User[i].name) / 2 + 120 - (6 - i) * 40))
				{
					FlushMouseMsgBuffer();
					No_v2 = 0;
					count = i;
					strcpy(ch20, User[i].name);
				}
				FlushMouseMsgBuffer();

				MOUSEMSG mouse2 = GetMouseMsg();
				if (mouse2.uMsg == WM_MOUSEMOVE)
					if (mouse1.x >= (Width / 2 - textwidth(User[i].name) / 2) && mouse1.y >= (Height / 2 - textheight(User[i].name) / 2 + 120 - (6 - i) * 40)
						&& mouse1.x <= (Width / 2 + textwidth(User[i].name) / 2) && mouse1.y <= (Height / 2 + textheight(User[i].name) / 2 + 120 - (6 - i) * 40))
						settextcolor(YELLOW);
					else
						settextcolor(BLACK);

				settextstyle(35, 20, "楷体");
				setbkmode(TRANSPARENT);
				outtextxy(Width / 2 - textwidth(User[i].name) / 2, Height / 2 - textheight(User[i].name) / 2 + 120 - (6 - i) * 40, User[i].name);
				settextcolor(BLACK);

				FlushMouseMsgBuffer();
			}
		}
	}
}
void View3()
{
	strcpy(v3name, ch20);

	initgraph(Width, Height);
	loadimage(&photo3, _T("..\\source\\love.jpg"), Width, Height);//背景图片
	putimage(0, 0, &photo3);

	settextcolor(BLACK);
	settextstyle(35, 20, "楷体");
	setbkmode(TRANSPARENT);
	outtextxy(Width / 2 - textwidth(arr3) / 2, Height / 2 - textheight(arr3) / 2 - 70, arr3);
	outtextxy(Width / 2 - textwidth(arr4) / 2, Height / 2 - textheight(arr4) / 2 + 70, arr4);
	settextstyle(17, 10, "宋体");
	outtextxy(10, 10, arr5);
	setfillcolor(WHITE);
	solidrectangle(10, 10 + textheight(arr5) + 5, 170, 10 + textheight(arr5) + 5 + 20);
	outtextxy(10, 10 + textheight(arr5) + 5 + 20 - textheight(v3name), v3name);
	while (1)
	{
		if (_kbhit())
		{
			v3ch = _getch();
			if (((v3ch >= '0') && (v3ch <= '9')) || ((v3ch >= 'a') && (v3ch <= 'z')) || ((v3ch >= 'A') && (v3ch <= 'Z')))
			{
				if (strlen(v3name) <= 20 - 3)
				{
					v3name[strlen(v3name)] = v3ch;
					v3name[strlen(v3name) + 1] = '\0';
				}
			}
			if (strlen(v3name) > 0)
			{
				if (v3ch == VK_BACK)
				{
					v3name[strlen(v3name) - 1] = '\0';
					setfillcolor(WHITE);
					solidrectangle(10, 10 + textheight(arr5) + 5, 170, 10 + textheight(arr5) + 5 + 20);
				}
			}
			settextstyle(17, 10, "宋体");
			outtextxy(10, 10 + textheight(arr5) + 5 + 20 - textheight(v3name), v3name);

			FILE* fp;
			struct user User[5];
			int i;
			if ((fp = fopen("..\\source\\username.txt", "r")) == NULL)
			{
				for (i = 0; i < 5; i++)
				{
					strcpy(User[i].name, "ERROR");
				}
			}
			else
			{
				for (i = 0; i < 5; i++)
				{
					fscanf(fp, "%s", User[i].name);
				}
			}
			fclose(fp);
			fflush(fp);

			strcpy(User[count].name, v3name);

			fp = fopen("..\\source\\username.txt", "w");
			for (i = 0; i < 5; i++)
			{
				fprintf(fp, "%s\n", User[i].name);
			}

			fclose(fp);
		}
		if (MouseHit())
		{
			MOUSEMSG mouse1 = GetMouseMsg();
			if (mouse1.mkLButton && mouse1.x >= (Width / 2 - textwidth(arr4) / 2) && mouse1.y >= (Height / 2 - textheight(arr4) / 2 + 70)
				&& mouse1.x <= (Width / 2 + textwidth(arr4) / 2) && mouse1.y <= (Height / 2 + textheight(arr4) / 2 + 70))
			{
				FlushMouseMsgBuffer();
				backv4 = 1;
				v4open = 1;
				break;
			}
			FlushMouseMsgBuffer();

			//MOUSEMSG mouse2 = GetMouseMsg();
			if (mouse1.uMsg == WM_MOUSEMOVE)
				if (mouse1.x >= (Width / 2 - textwidth(arr4) / 2) && mouse1.y >= (Height / 2 - textheight(arr4) / 2 + 70)
					&& mouse1.x <= (Width / 2 + textwidth(arr4) / 2) && mouse1.y <= (Height / 2 + textheight(arr4) / 2 + 70))
					settextcolor(YELLOW);
				else
					settextcolor(BLACK);

			settextstyle(35, 20, "楷体");
			setbkmode(TRANSPARENT);
			outtextxy(Width / 2 - textwidth(arr4) / 2, Height / 2 - textheight(arr4) / 2 + 70, arr4);
			settextcolor(BLACK);
			FlushMouseMsgBuffer();

			//MOUSEMSG mouse1 = GetMouseMsg();
			if (mouse1.mkLButton && mouse1.x >= (Width / 2 - textwidth(arr3) / 2) && mouse1.y >= (Height / 2 - textheight(arr3) / 2 - 70)
				&& mouse1.x <= (Width / 2 + textwidth(arr3) / 2) && mouse1.y <= (Height / 2 + textheight(arr3) / 2 - 70))
			{
				FlushMouseMsgBuffer();
				No = 1;
				break;
			}
			FlushMouseMsgBuffer();
			//MOUSEMSG mouse2 = GetMouseMsg();
			if (mouse1.uMsg == WM_MOUSEMOVE)
				if (mouse1.x >= (Width / 2 - textwidth(arr3) / 2) && mouse1.y >= (Height / 2 - textheight(arr3) / 2 - 70)
					&& mouse1.x <= (Width / 2 + textwidth(arr3) / 2) && mouse1.y <= (Height / 2 + textheight(arr3) / 2 - 70))
					settextcolor(YELLOW);
				else
					settextcolor(BLACK);

			settextstyle(35, 20, "楷体");
			setbkmode(TRANSPARENT);
			outtextxy(Width / 2 - textwidth(arr3) / 2, Height / 2 - textheight(arr3) / 2 - 70, arr3);
			settextcolor(BLACK);
			FlushMouseMsgBuffer();
		}


	}
}
void View4()
{
	initgraph(Width, Height);
	loadimage(&photo4, _T("..\\source\\love.jpg"), Width, Height);
	putimage(0, 0, &photo4);

	FILE* fp;
	int i;
	struct LIST list[10];
	fp = fopen("..\\source\\list.txt", "r");
	for (i = 0; i < 10; i++)
	{
		fscanf(fp, "%s%d", list[i].name, &list[i].score);
	}
	fclose(fp);
	settextcolor(BLACK);
	settextstyle(35, 20, "宋体");
	setbkmode(TRANSPARENT);
	outtextxy(260, 50, arr6);
	while (backv4)
	{

		for (i = 0; i < 10; i++)
		{
			settextstyle(17, 10, "楷体");
			setbkmode(TRANSPARENT);
			outtextxy(160, 100 + i * 35, list[i].name);

			_stprintf(list[i].Score, _T("%d"), list[i].score);
			outtextxy(360, 100 + i * 35, list[i].Score);
			//outtextxy(360, 100 + i * 50, list[i].score);
		}

		if (MouseHit())
		{
			settextstyle(17, 10, "楷体");
			setbkmode(TRANSPARENT);
			outtextxy(10, 10, arr7);

			MOUSEMSG mouse1 = GetMouseMsg();
			if (mouse1.mkLButton && mouse1.x >= 10 && mouse1.y >= 10
				&& mouse1.x <= (10 + textwidth(arr7)) && mouse1.y <= (10 + textheight(arr7)))
			{
				FlushMouseMsgBuffer();
				v4open = 0;
				backv4 = 0;
			}

			//MOUSEMSG mouse2 = GetMouseMsg();
			if (mouse1.uMsg == WM_MOUSEMOVE)
				if (mouse1.mkLButton && mouse1.x >= 10 && mouse1.y >= 10
					&& mouse1.x <= (10 + textwidth(arr7)) && mouse1.y <= (10 + textheight(arr7)))
					settextcolor(YELLOW);
				else
					settextcolor(BLACK);
			FlushMouseMsgBuffer();
		}
	}
}