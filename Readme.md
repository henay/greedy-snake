### 使用软件

Microsoft Visual Studio Community 2022 (64 位) - Current
版本 17.3.6

### 关于报错

参考文档

* https://blog.csdn.net/qq_35307005/article/details/87906208

* https://blog.csdn.net/qq_44256828/article/details/89279697

* https://blog.csdn.net/love_0_love/article/details/120024094

### 运行说明

<img src="img/image-20230810195227772.png" alt="image-20230810195227772" style="zoom:67%;" />

​	进入主函数后运行界面1和界面2，然后进入while循环，初始化变量，进入view3（）主界面，输入账户名，点击开始游戏，进入游戏

<img src="img/image-20230810195513266.png" alt="image-20230810195513266" style="zoom:33%;" /><img src="img/image-20230810195735263.png" alt="image-20230810195735263" style="zoom:33%;" />

​	到达指定分数时，速度加快

<img src="img/image-20230810195905269.png" alt="image-20230810195905269" style="zoom: 67%;" />

​	撞墙后死亡，回到主界面

​	点击排行榜查看历史记录



​	点击back后返回主界面

<img src="img/image-20230810200116131.png" alt="image-20230810200116131" style="zoom: 33%;" />

#### 资源文件

​	函数中使用相对路径，放在source文件夹下